package com.nichol.service;

import java.util.List;

import com.nichol.pojo.Admin;
import com.nichol.pojo.Course;
import com.nichol.pojo.Student;
import com.nichol.pojo.Teacher;

/*
 * @Author: 姜东浩
 * @Date: 2021-11-30 21:07:12
 * @Description: 管理员业务接口类
 */
public interface AdminService {

    /**
     * @description: 生成一位管理员
     * @param {*}
     * @return {String} 操作提示信息
     * @author: 姜东浩
     */
    public abstract String initAdmin();

    /**
     * @description: 登录
     * @param {Admin} admin
     * @return {*} 登陆对象
     * @author: 姜东浩
     */
    public abstract Admin login(Admin admin);

    /**
     * @description: 修改管理员密码
     * @param {String} pwd 密码
     * @return {int} 成功1，失败-1
     * @author: 姜东浩
     */
    public abstract int updatePwd(String pwd);

    // =========================================================================================================
    
    /**
     * @description: 添加一名教师
     * @param {Teacher} teacher
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */
    public abstract int insertOneTeacher(Teacher teacher);

    /**
     * @description: 删除一名教师
     * @param {String} tid 教师ID
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */
    public abstract int deleteOneTeacher(String tid);

    /**
     * @description: 修改教师密码
     * @param {String} tid 教师ID
     * @param {String} pwd 新密码
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */
    public abstract int updateTeacherPwd(String tid, String pwd);

    /**
     * @description: 设置教师所授课程
     * @param {String} tid 教师ID
     * @param {String} cid 课程ID
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */
    public abstract int updateTeacherCid(String tid, String cid);

    /**
     * @description: 查询所有教师
     * @param {*}
     * @return {*} 教师集合
     * @author: 姜东浩
     */
    public abstract List<Teacher> selectAllTeachers();

    /**
     * @description: 查询一名教师
     * @param {String} tid 教师ID
     * @return {*} 被查询的教师
     * @author: 姜东浩
     */
    public abstract Teacher selectTeacher(String tid);

    // =========================================================================================================
    
    /**
     * @description: 新增一门课程
     * @param {Course} course
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */
    public abstract int insertOneCourse(Course course);

    /**
     * @description: 删除一门课程
     * @param {String} cid 课程编号
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */    
    public abstract int deleteOneCourse(String cid);

    /**
     * @description: 查询所有课程
     * @param {*}
     * @return {*} 课程集合
     * @author: 姜东浩
     */      
    public abstract List<Course> selectAllCourses();

    // =========================================================================================================
 
    /**
     * @description: 添加一名学生
     * @param {Student} student
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */    
    public abstract int insertOneStudent(Student student);

    /**
     * @description: 删除一名学生
     * @param {String} sid 学生编号
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */    
    public abstract int deleteOneStudent(String sid);

    /**
     * @description: 修改学生密码
     * @param {String} sid 学生编号
     * @param {String} pwd 新密码
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */    
    public abstract int updateStudentPwd(String sid,String pwd);

    /**
     * @description: 查询所有学生
     * @param {*}
     * @return {*} 学生集合
     * @author: 姜东浩
     */    
    public abstract List<Student> selectALlStudents();

}