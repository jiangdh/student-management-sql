package com.nichol.service.impl;

import java.sql.SQLException;
import java.util.List;

import com.nichol.dao.AdminDAO;
import com.nichol.dao.impl.AdminDAOImpl;
import com.nichol.pojo.Admin;
import com.nichol.pojo.Course;
import com.nichol.pojo.Student;
import com.nichol.pojo.Teacher;
import com.nichol.service.AdminService;
import com.nichol.util.JDBCUtil;

/*
 * @Author: 姜东浩
 * @Date: 2021-11-30 21:08:28
 * @Description: 管理员业务操作实现类
 */
public class AdminServiceImpl implements AdminService {

    AdminDAO adminDAO = new AdminDAOImpl();

    /**
     * @description: 生成一位管理员账号
     * @param {*}
     * @return {*} 操作提示信息
     * @author: 姜东浩
     */
    @Override
    public String initAdmin() {
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            adminDAO.initAdmin();
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return "ERROE : 生成管理员账号失败!";
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return "SUCCESS : 生成管理员账号成功!";
    }

    /**
     * @description: 管理员登录
     * @param {Admin} admin
     * @return {*} 登录对象
     * @author: 姜东浩
     */
    @Override
    public Admin login(Admin admin) {
        try {
            return adminDAO.login(admin);
        } catch (SQLException e) {
            return null;
        }
    }

    /**
     * @description: 修改管理员密码
     * @param {String} pwd 密码
     * @return {int} 成功1，失败-1
     * @author: 姜东浩
     */
    @Override
    public int updatePwd(String pwd) {
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            adminDAO.updatePwd(pwd);
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return -1;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return 1;
    }

    // =========================================================================================================
    
    /**
     * @description: 添加一名教师
     * @param {Teacher} teacher
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */
    @Override
    public int insertOneTeacher(Teacher teacher) {
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            adminDAO.insertOneTeacher(teacher);
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return -1;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return 1;
    }

    /**
     * @description: 删除一名教师
     * @param {String} tid 教师ID
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */
    @Override
    public int deleteOneTeacher(String tid) {
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            adminDAO.deleteOneTeacher(tid);
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return -1;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return 1;
    }

    /**
     * @description: 修改教师密码
     * @param {String} tid 教师ID
     * @param {String} pwd 新密码
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */
    @Override
    public int updateTeacherPwd(String tid, String pwd) {
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            adminDAO.updateTeacherPwd(tid, pwd);
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return -1;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return 1;
    }

    /**
     * @description: 设置教师所授课程
     * @param {String} tid 教师ID
     * @param {String} cid 课程ID
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */
    @Override
    public int updateTeacherCid(String tid, String cid) {
        Teacher teacher = null;
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            teacher = adminDAO.selectTeacher(tid);
            if (teacher == null || teacher.getcId() != null) {
                return -1;
            }
            adminDAO.updateTeacherCid(tid, cid);
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return -1;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return 1;
    }

    /**
     * @description: 询所有教师
     * @param {*}
     * @return {*} 教师集合
     * @author: 姜东浩
     */
    @Override
    public List<Teacher> selectAllTeachers() {
        List<Teacher> teacherList = null;
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            teacherList = adminDAO.selectAllTeachers();
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return teacherList;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return teacherList;
    }

    /**
     * @description: 根据教师ID查询一名教师
     * @param {String} tid 教师ID
     * @return {*} 被查询的教师
     * @author: 姜东浩
     */
    @Override
    public Teacher selectTeacher(String tid) {
        Teacher teacher = null;
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            teacher = adminDAO.selectTeacher(tid);
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return teacher;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return teacher;
    }

    // =========================================================================================================
    
    /**
     * @description: 新增一门课程
     * @param {Course} course
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */
    @Override
    public int insertOneCourse(Course course) {
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            adminDAO.insertOneCourse(course);
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return -1;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return 1;
    }

    /**
     * @description: 删除一门课程
     * @param {String} cid 课程编号
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */
    @Override
    public int deleteOneCourse(String cid) {
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            adminDAO.deleteOneCourse(cid);
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return -1;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return 1;
    }

    /**
     * @description: 查询所有课程
     * @param {*}
     * @return {*} 课程集合
     * @author: 姜东浩
     */
    @Override
    public List<Course> selectAllCourses() {
        List<Course> courseList = null;
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            courseList = adminDAO.selectAllCourses();
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return courseList;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return courseList;
    }

    // =========================================================================================================

    /**
     * @description: 添加一名学生
     * @param {Student} student
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */    
    @Override
    public int insertOneStudent(Student student) {
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            adminDAO.insertOneStudent(student);
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return -1;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return 1;
    }

    /**
     * @description: 删除一名学生
     * @param {String} sid
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */  
    @Override  
    public int deleteOneStudent(String sid) {
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            adminDAO.deleteOneStudent(sid);
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return -1;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return 1;
    }

    /**
     * @description: 修改学生密码
     * @param {String} sid 学生编号
     * @param {String} pwd 新密码
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */  
    @Override  
    public int updateStudentPwd(String sid, String pwd) {
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            adminDAO.updateStudentPwd(sid, pwd);
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return -1;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return 1;
    }

    /**
     * @description: 查询所有学生
     * @param {*}
     * @return {*} 学生集合
     * @author: 姜东浩
     */    
    @Override
    public List<Student> selectALlStudents() {
        List<Student> studentList = null;
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            studentList = adminDAO.selectALlStudents();
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return studentList;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return studentList;
    }

}