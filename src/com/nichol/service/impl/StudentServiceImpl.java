package com.nichol.service.impl;

import java.sql.SQLException;
import java.util.List;

import com.nichol.dao.StudentDAO;
import com.nichol.dao.impl.StudentDAOImpl;
import com.nichol.pojo.Course;
import com.nichol.pojo.Student;
import com.nichol.service.StudentService;
import com.nichol.util.JDBCUtil;

/*
 * @Author: 姜东浩
 * @Date: 2021-12-01 22:41:53
 * @Description: 学生业务实现类
 */
public class StudentServiceImpl implements StudentService{

    StudentDAO studentDAO = new StudentDAOImpl();

    /**
     * @description: 学生登录
     * @param {Student} student
     * @return {*} 登陆对象
     * @author: 姜东浩
     */
    @Override 
    public Student login(Student student) {
        try {
            return studentDAO.login(student);
        } catch (SQLException e) {
            return null;
        }
    }

    /**
     * @description: 学生选课
     * @param {String} cid
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */    
    @Override
    public int chooseCourse(String cid) {
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            Course course = studentDAO.selectCourseById(cid);
            if(course == null){
                return -1;
            }
            int num = course.getNum();
            int snum = course.getSnum();
            if(!(snum>num)){
                return -1;
            }
            studentDAO.updateCourseNum("+", cid);
            studentDAO.chooseCourse(cid);
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return -1;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return 1;
        
    }

    /**
     * @description: 删除课程
     * @param {String} cid 课程编号
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */   
    @Override 
    public int deleteCourse(String cid) {
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            Course course = studentDAO.selectCourseById(cid);
            if(course == null){
                return -1;
            }

            if(studentDAO.deleteCourse(cid)!=1){
                return -1;
            }
            studentDAO.updateCourseNum("-", cid);
            
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return -1;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return 1;
    }

    /**
     * @description: 显示所有课程
     * @param {*}
     * @return {*}
     * @author: 姜东浩
     */    
    @Override
    public List<Course> selectAllCourse() {
        List<Course> courseList = null;
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            courseList = studentDAO.selectAllCourse();
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return courseList;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return courseList;
    }

    /**
     * @description: 查看已选课程
     * @return {*} 已选课程集合
     * @author: 姜东浩
     */  
    @Override  
    public List<Course> selectAllChooseCourse() {
        List<Course> courseList = null;
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            courseList = studentDAO.selectAllChooseCourse();
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return courseList;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return courseList;
    }

     /**
     * @description: 修改学生密码
     * @param {String} pwd 新密码
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */  
    @Override
    public int updatePassword(String pwd) {
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            studentDAO.updatePassword(pwd);
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return -1;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return 1;
    }
    
}