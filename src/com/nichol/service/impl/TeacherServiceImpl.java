package com.nichol.service.impl;

import java.sql.SQLException;
import java.util.List;

import com.nichol.dao.TeacherDAO;
import com.nichol.dao.impl.TeacherDAOImpl;
import com.nichol.pojo.Course;
import com.nichol.pojo.Student;
import com.nichol.pojo.Teacher;
import com.nichol.service.TeacherService;
import com.nichol.util.JDBCUtil;

/*
 * @Author: 姜东浩
 * @Date: 2021-12-02 00:04:02
 * @Description: 教师业务实现类
 */
public class TeacherServiceImpl implements TeacherService {

    TeacherDAO teacherDAO = new TeacherDAOImpl();

    /**
     * @description: 教师登录
     * @param {Teacher} teacher 登录教师对象
     * @return {*} 登陆进来的教师对象
     * @author: 姜东浩
     */
    @Override
    public Teacher login(Teacher teacher) {
        try {
            return teacherDAO.login(teacher);
        } catch (SQLException e) {
            return null;
        }
    }

    /**
     * @description: 登分
     * @param {String} sid 学生编号
     * @param {double} grade 成绩
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */
    @Override
    public int insertStudentGrade(String sid, double grade) {
        int count = -1;
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            count = teacherDAO.insertStudentGrade(sid, grade);
            // 提交事务
            JDBCUtil.getConn().commit();
            if (count == 1) {
                return 1;
            } else {
                return -1;
            }
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return -1;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
    }

    /**
     * @description: 改分
     * @param {String} sid 学生编号
     * @param {double} grade 成绩
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */
    @Override
    public int updateStudentGrade(String sid, double grade) {
        int count = -1;
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            count = teacherDAO.updateStudentGrade(sid, grade);
            // 提交事务
            JDBCUtil.getConn().commit();
            if (count == 1) {
                return 1;
            } else {
                return -1;
            }
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return -1;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
    }

    /**
     * @description: 查看学生集合
     * @return {*} 管理的学生集合
     * @author: 姜东浩
     */
    @Override
    public List<Student> selectStudentList() {
        List<Student> studentList = null;
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            studentList = teacherDAO.selectStudentList();
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return studentList;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return studentList;
    }

    /**
     * @description: 查看自己所授科目
     * @return {*} 自己所授科目对象
     * @author: 姜东浩
     */
    @Override
    public Course selectMyCourse() {
        Course course = null;
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            course = teacherDAO.selectMyCourse();
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return course;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return course;

    }

    /**
     * @description: 修改密码
     * @param {String} pwd 新密码
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */
    @Override
    public int updatePassword(String pwd) {
        try {
            // 开启事务
            JDBCUtil.getConn().setAutoCommit(false);
            teacherDAO.updatePassword(pwd);
            // 提交事务
            JDBCUtil.getConn().commit();
        } catch (SQLException e) {
            try {
                // 回滚事务
                JDBCUtil.getConn().rollback();
            } catch (SQLException e1) {
            }
            return -1;
        } finally {
            // 关闭连接
            JDBCUtil.closeConn();
        }
        return 1;

    }

}