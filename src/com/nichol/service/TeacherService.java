package com.nichol.service;

import java.util.List;

import com.nichol.pojo.Course;
import com.nichol.pojo.Student;
import com.nichol.pojo.Teacher;

/*
 * @Author: 姜东浩
 * @Date: 2021-12-02 00:03:01
 * @Description: 教师业务接口
 */
public interface TeacherService {
    
    /**
     * @description: 教师登录
     * @param {Teacher} teacher
     * @return {*} 登录教师对象
     * @author: 姜东浩
     */    
    public abstract Teacher login(Teacher teacher);

    /**
     * @description: 登分
     * @param {String} sid
     * @param {String} cid
     * @param {double} grade
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */
    public abstract int insertStudentGrade(String sid, double grade);


    /**
     * @description: 改分
     * @param {String} sid
     * @param {String} cid
     * @param {double} grade
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */    
    public abstract int updateStudentGrade(String sid , double grade);
    
    /**
     * @description: 查看自己的学生列表
     * @return {*} 学生集合
     * @author: 姜东浩
     */    
    public abstract List<Student> selectStudentList();
    
    /**
     * @description: 查看自己所授课程
     * @return {*} 课程对象
     * @author: 姜东浩
     */    
    public abstract Course selectMyCourse();

    /**
     * @description: 修改密码
     * @param {String} pwd
     * @return {*} 成功1，失败-1
     * @author: 姜东浩
     */    
    public abstract int updatePassword(String pwd);
    
}