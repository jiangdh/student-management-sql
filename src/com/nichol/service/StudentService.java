package com.nichol.service;

import java.util.List;

import com.nichol.pojo.Course;
import com.nichol.pojo.Student;

/*
 * @Author: 姜东浩
 * @Date: 2021-12-01 22:41:32
 * @Description: 学生业务接口
 */
public interface StudentService {
    
    /**
     * @description: 学生登录
     * @param {Student} student
     * @return {*} 登陆对象
     * @author: 姜东浩
     */    
    public abstract Student login(Student student);

    /**
     * @description: 学生选课
     * @param {String} cid 课程编号
     * @return {*}
     * @author: 姜东浩
     */    
    public abstract int chooseCourse(String cid);

    /**
     * @description: 删除选课
     * @param {String} cid 课程编号
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */      
    public abstract int deleteCourse(String cid);

    /**
     * @description: 查看所有课程
     * @param {*}
     * @return {*} 课程集合
     * @author: 姜东浩
     */    
    public abstract List<Course> selectAllCourse(); 

     /**
     * @description: 查看已选课程
     * @return {*}课程集合
     * @author: 姜东浩
     */      
    public abstract List<Course> selectAllChooseCourse();

    /**
     * @description: 修改密码
     * @param {String} pwd 新密码
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */    
    public abstract int updatePassword(String pwd);
 
}