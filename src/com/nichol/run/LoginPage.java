package com.nichol.run;

import java.util.Scanner;

import com.nichol.util.InputTrans;
import com.nichol.view.ChooseInto;

/*
 * @Author: 姜东浩
 * @Date: 2021-11-30 11:17:56
 * @Description: 程序主入口
 */
public class LoginPage {
    
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        do{
            System.out.println("**********登录窗口*********");
            System.out.println("* 1、登录                *");
            System.out.println("* 2、退出                *");
            System.out.println("*************************");
            switch(InputTrans.betweenStatedInt(sc.next())){
                case 1: // 用户登录
                    System.out.println("请输入账号：");
                    String userName = sc.next();
                    System.out.println("请输入密码：");
                    String password = sc.next();
                    String sign = InputTrans.judgeTA(userName);
                    ChooseInto.gotoPage(sign,userName,password);
                    break;
                case 2: // 退出系统
                    System.out.println("感谢您的使用，再见");
                    System.exit(0);
                default: // 输入有误
                    System.out.println("ERROR: 您的输入有误，请重新选择功能!");
                    break; 
            }
        }while(true);
        
    }
    
}