package com.nichol.dao;

import java.sql.SQLException;
import java.util.List;

import com.nichol.pojo.Admin;
import com.nichol.pojo.Course;
import com.nichol.pojo.Student;
import com.nichol.pojo.Teacher;

/*
 * @Author: 姜东浩
 * @Date: 2021-11-30 21:01:07
 * @Description: 管理员数据库操作接口
 */
public interface AdminDAO {

    /**
     * @description: 生成一位管理员
     * @param {*}
     * @return {*} 数据库变更数量
     * @author: 姜东浩
     */    
    public abstract int initAdmin() throws SQLException;
 
    /**
     * @description: 登录
     * @param {Admin} admin
     * @return {*} 登陆对象
     * @author: 姜东浩
     */     
    public abstract Admin login(Admin admin) throws SQLException;

    /**
     * @description: 修改管理员密码
     * @param {String} pwd 密码
     * @return {*} 数据库变更数量
     * @author: 姜东浩
     */     
    public abstract int updatePwd(String pwd) throws SQLException;

    // =========================================================================================================
   
    /**
     * @description: 添加一名教师
     * @param {Teacher} teacher
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */    
    public abstract int insertOneTeacher(Teacher teacher) throws SQLException;

    /**
     * @description: 删除一名教师
     * @param {String} tid 教师ID
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */    
    public abstract int deleteOneTeacher(String tid) throws SQLException;

    /**
     * @description: 修改教师密码
     * @param {String} tid 教师ID
     * @param {String} pwd 新密码
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */
    public abstract int updateTeacherPwd(String tid,String pwd) throws SQLException;

    /**
     * @description: 设置教师所授课程
     * @param {String} tid 教师ID
     * @param {String} cid 课程ID
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */    
    public abstract int updateTeacherCid(String tid,String cid) throws SQLException;

    /**
     * @description: 查询所有教师
     * @param {*}
     * @return {*} 教师集合
     * @author: 姜东浩
     */    
    public abstract List<Teacher> selectAllTeachers() throws SQLException;

    /**
     * @description: 查询一名教师
     * @param {String} tid 教师ID
     * @return {*} 被查询的教师
     * @author: 姜东浩
     */       
    
     public abstract Teacher selectTeacher(String tid) throws SQLException;
    // =========================================================================================================
   
    /**
     * @description: 新增一门课程
     * @param {Course} course
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */    
    public abstract int insertOneCourse(Course course) throws SQLException;

    /**
     * @description: 删除一门课程
     * @param {String} cid 课程编号
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */    
    public abstract int deleteOneCourse(String cid) throws SQLException;

    /**
     * @description: 查询所有课程
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */    
     public abstract List<Course> selectAllCourses() throws SQLException;
    
     // =========================================================================================================

    /**
     * @description: 添加一名学生
     * @param {Student} student
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */    
    public abstract int insertOneStudent(Student student) throws SQLException;

    /**
     * @description: 删除一名学生
     * @param {String} sid 学生编号
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */    
    public abstract int deleteOneStudent(String sid) throws SQLException;

    /**
     * @description: 修改学生密码
     * @param {String} sid 学生编号
     * @param {String} pwd 新密码
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */    
    public abstract int updateStudentPwd(String sid,String pwd) throws SQLException;

    /**
     * @description: 查询所有学生
     * @param {*}
     * @return {*} 学生集合
     * @author: 姜东浩
     */    
    public abstract List<Student> selectALlStudents() throws SQLException;

}