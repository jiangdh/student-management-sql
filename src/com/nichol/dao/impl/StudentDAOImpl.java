package com.nichol.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.nichol.dao.StudentDAO;
import com.nichol.pojo.Course;
import com.nichol.pojo.Student;
import com.nichol.util.JDBCUtil;
import com.nichol.util.MD5;

/*
 * @Author: 姜东浩
 * @Date: 2021-12-01 19:48:32
 * @Description: 学生数据操作实现类
 */
public class StudentDAOImpl implements StudentDAO {

    private static Student stu = null;

    /**
     * @description:学生登录
     * @param {Student} student
     * @return {*} 登陆对象
     * @author: 姜东浩
     */
    @Override
    public Student login(Student student) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "select * from user where id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, student.getStudentId());
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            if (rs.getString("pwd").equals(MD5.md5(student.getPassword()))) {
                stu = student;
            }
        }
        return stu;
    }

    /**
     * @description: 学生选课
     * @param {String} cid 课程编号
     * @return {*}
     * @author: 姜东浩
     */    
    @Override
    public int chooseCourse(String cid) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "insert into sc(sid,cid) values (?, ?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, stu.getStudentId());
        stmt.setString(2, cid);
        int count = stmt.executeUpdate();
        return count;
    }

   /**
     * @description: 删除选课
     * @param {String} cid 课程编号
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */      
    @Override
    public int deleteCourse(String cid) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "delete from sc where sid=? and cid=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, stu.getStudentId());
        stmt.setString(2, cid);
        int count = stmt.executeUpdate();
        return count;
    }

    /**
     * @description: 查看已选课程
     * @return {*}课程集合
     * @author: 姜东浩
     */      
    @Override
    public List<Course> selectAllChooseCourse() throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "select sc.cid, course.cname, sc.grade from sc inner join course on  sc.cid = course.id where sc.cid = course.id and sid = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, stu.getStudentId());
        ResultSet rs = stmt.executeQuery();
        List<Course> courseList = new ArrayList<Course>();
        while (rs.next()) {
            Course course = new Course();
            course.setId(rs.getString("sc.cid"));
            course.setCname(rs.getString("course.cname"));
            course.setGrade(rs.getDouble("sc.grade"));
            courseList.add(course);
        }
        return courseList;
    }

    /**
     * @description: 查看所有课程
     * @param {*}
     * @return {*} 课程集合
     * @author: 姜东浩
     */
    @Override
    public List<Course> selectAllCourse() throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "select id,cname,num,snum from course";
        PreparedStatement stmt = conn.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();
        List<Course> courseList = new ArrayList<Course>();
        while (rs.next()) {
            Course course = new Course();
            course.setId(rs.getString("id"));
            course.setCname(rs.getString("cname"));
            course.setNum(rs.getInt("num"));
            course.setSnum(rs.getInt("snum"));
            courseList.add(course);
        }
        return courseList;
    }

    /**
     * @description: 修改密码
     * @param {String} pwd 新密码
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */
    @Override
    public int updatePassword(String pwd) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "update student set pwd =? where id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, MD5.md5(pwd));
        stmt.setString(2, stu.getStudentId());
        int count = stmt.executeUpdate();
        return count;
    }

    /**
     * @description: 选课人数操作
     * @param {String} oper 表示人数增加还是减少
     * @param {String} cid 课程编号
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */  
    @Override
    public int updateCourseNum(String oper,String cid) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = null;
        if("+".equals(oper)){
           sql = "update course set num=num+1 where id=?";
        }else if("-".equals(oper)){
            sql = "update course set num=num-1 where id=?";
        }else{
            return 0;
        }
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, cid);
        int count = stmt.executeUpdate();
        return count;
    }

    /**
     * @description: 根据课程编号查找指定课程
     * @param {String} cid
     * @return {*} 被查找课程
     * @author: 姜东浩
     */   
    @Override 
    public Course selectCourseById(String cid) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "select id,cname,num,snum from course where id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, cid);
        ResultSet rs = stmt.executeQuery();
        Course course = new Course();
        while (rs.next()) {
            course.setId(rs.getString("id"));
            course.setCname(rs.getString("cname"));
            course.setNum(rs.getInt("num"));
            course.setSnum(rs.getInt("snum"));
        }
        return course;
    }

}