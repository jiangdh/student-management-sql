package com.nichol.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.nichol.dao.TeacherDAO;
import com.nichol.pojo.Course;
import com.nichol.pojo.Student;
import com.nichol.pojo.Teacher;
import com.nichol.util.JDBCUtil;
import com.nichol.util.MD5;

/*
 * @Author: 姜东浩
 * @Date: 2021-12-01 23:28:37
 * @Description: 教师数据操作实现类
 */
public class TeacherDAOImpl implements TeacherDAO {

    private static Teacher tea = null;

    /**
     * @description: 教师登录
     * @param {Teacher} teacher
     * @return {*} 登录的教师对象
     * @author: 姜东浩
     */
    @Override
    public Teacher login(Teacher teacher) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "select * from user where id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, teacher.getTeacherId());
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            if (rs.getString("pwd").equals(MD5.md5(teacher.getPassword()))) {
                tea = teacher;
            }
        }
        return tea;
    }

    /**
     * @description: 登分
     * @param {String} sid
     * @param {double} grade
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */    
    public int insertStudentGrade(String sid, double grade) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "insert into sc(sid,cid,grade) values(?,(select t.cid from teacher t where t.id=?),?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, sid);
        stmt.setString(2, tea.getTeacherId());
        stmt.setDouble(3, grade);
        int count = stmt.executeUpdate();
        return count;
    }
    
    /**
     * @description: 改分
     * @param {String} sid
     * @param {double} grade
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */
    @Override
    public int updateStudentGrade(String sid, double grade) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "update sc set grade = ? where sid = ? and cid =(select t.cid from teacher t where t.id=?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setDouble(1, grade);
        stmt.setString(2, sid);
        stmt.setString(3, tea.getTeacherId());
        int count = stmt.executeUpdate();
        return count;
    }

    /**
     * @description: 查看自己的学生列表
     * @return {*} 学生集合
     * @author: 姜东浩
     */
    @Override
    public List<Student> selectStudentList() throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "select s.id,s.sname,s.sex,sc.grade from student s ,sc where sc.sid=s.id and  sc.cid =(select t.cid from teacher t where t.id=?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, tea.getTeacherId());
        ResultSet rs = stmt.executeQuery();
        List<Student> studentList = new ArrayList<Student>();
        while (rs.next()) {
            Student student = new Student();
            student.setStudentId(rs.getString("s.id"));
            student.setSname(rs.getString("s.sname"));
            student.setSex(rs.getString("s.sex"));
            student.setGrade(rs.getDouble("sc.grade"));
            studentList.add(student);
        }
        return studentList;
    }

    /**
     * @description: 查看自己所授课程
     * @return {*} 课程对象
     * @author: 姜东浩
     */
    @Override
    public Course selectMyCourse() throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "select id,cname,num,snum from course where id =(select cid from teacher where id = ?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, tea.getTeacherId());
        ResultSet rs = stmt.executeQuery();
        Course course = new Course();
        while (rs.next()) {
            course.setId(rs.getString("id"));
            course.setCname(rs.getString("cname"));
            course.setNum(rs.getInt("num"));
            course.setSnum(rs.getInt("snum"));
        }
        return course;
    }

    /**
     * @description: 修改密码
     * @param {String} pwd 新密码
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */
    @Override
    public int updatePassword(String pwd) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "update teacher set pwd = ? where id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, MD5.md5(pwd));
        stmt.setString(2, tea.getTeacherId());
        int count = stmt.executeUpdate();
        return count;
    }

}