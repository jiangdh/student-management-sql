package com.nichol.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.nichol.dao.AdminDAO;
import com.nichol.pojo.Admin;
import com.nichol.pojo.Course;
import com.nichol.pojo.Student;
import com.nichol.pojo.Teacher;
import com.nichol.util.JDBCUtil;
import com.nichol.util.MD5;

/*
 * @Author: 姜东浩
 * @Date: 2021-11-30 21:03:31
 * @Description: 管理员数据库操作实现类
 */
public class AdminDAOImpl implements AdminDAO {

    private static Admin adm = null;

    /**
     * @description: 生成一位管理员
     * @param {*}
     * @return 数据库变更数量
     * @author: 姜东浩
     */
    @Override
    public int initAdmin() throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "insert into admin values(?,?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, "a01");
        stmt.setString(2, MD5.md5("admin"));
        int count = stmt.executeUpdate();
        return count;
    }

    /**
     * @description: 登录
     * @param {Admin} admin
     * @return {*} 登陆对象
     * @author: 姜东浩
     */
    @Override
    public Admin login(Admin admin) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "select * from user where id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, admin.getAdminId());
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            if (rs.getString("pwd").equals(MD5.md5(admin.getPassword()))) {
                adm = admin;
            }
        }
        return adm;
    }
    
    // =========================================================================================================
    
    /**
     * @description: 修改管理员密码
     * @param {String} pwd 密码
     * @return {*} 数据库变更数量
     * @author: 姜东浩
     */
    @Override
    public int updatePwd(String pwd) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "update admin set pwd =? where id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, MD5.md5(pwd));
        stmt.setString(2, adm.getAdminId());
        int count = stmt.executeUpdate();
        return count;
    }

    /**
     * @description: 添加一名教师
     * @param {Teacher} teacher
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */
    @Override
    public int insertOneTeacher(Teacher teacher) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "insert into teacher(id,tname,pwd,sex) values(?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, teacher.getTeacherId());
        stmt.setString(2, teacher.getTeacherName());
        stmt.setString(3, MD5.md5(teacher.getPassword()));
        stmt.setString(4, teacher.getSex());
        int count = stmt.executeUpdate();
        return count;
    }

    /**
     * @description: 删除一名教师
     * @param {String} tid 教师ID
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */
    @Override
    public int deleteOneTeacher(String tid) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "delete from teacher where id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, tid);
        int count = stmt.executeUpdate();
        return count;
    }

    /**
     * @description: 修改教师密码
     * @param {String} tid 教师ID
     * @param {String} pwd 新密码
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */
    @Override
    public int updateTeacherPwd(String tid, String pwd) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "update teacher set pwd =? where id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, MD5.md5(pwd));
        stmt.setString(2, tid);
        int count = stmt.executeUpdate();
        return count;
    }

     /**
     * @description: 设置教师所授课程
     * @param {String} tid 教师ID
     * @param {String} cid 课程ID
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */    
    @Override  
    public int updateTeacherCid(String tid,String cid) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "update teacher set cid =? where id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, cid);
        stmt.setString(2, tid);
        int count = stmt.executeUpdate();
        return count;
    }

    /**
     * @description: 查询所有教师信息
     * @param {*}
     * @return {*} 查询出来的教师集合
     * @author: 姜东浩
     */   
    @Override 
    public List<Teacher> selectAllTeachers() throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "select id,tname,sex,cid from teacher";
        PreparedStatement stmt = conn.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();
        List<Teacher> teacherList = new ArrayList<Teacher>();
        while (rs.next()) {
            Teacher teacher = new Teacher();
            teacher.setTeacherId(rs.getString("id"));
            teacher.setTeacherName(rs.getString("tname"));
            teacher.setSex(rs.getString("sex"));
            teacher.setcId(rs.getString("cid"));
            teacherList.add(teacher);
        }
        return teacherList;
    }

    /**
     * @description: 查询一名教师
     * @param {String} tid 教师ID
     * @return {*} 被查询的教师
     * @author: 姜东浩
     */     
    @Override
    public Teacher selectTeacher(String tid) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "select id,tname,sex,cid from teacher where id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, tid);
        ResultSet rs = stmt.executeQuery();
        Teacher teacher = new Teacher();
        while(rs.next()) {
            teacher.setTeacherId(rs.getString("id"));
            teacher.setTeacherName(rs.getString("tname"));
            teacher.setSex(rs.getString("sex"));
            teacher.setcId(rs.getString("cid"));
        }
        return teacher;
    }
    
    // =========================================================================================================
    
    /**
     * @description: 新增一门课程
     * @param {Course} course
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */  
    @Override   
    public int insertOneCourse(Course course) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "insert into course(id,cname,num,snum) values(?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, course.getId());
        stmt.setString(2, course.getCname());
        stmt.setInt(3, 0);
        stmt.setInt(4, course.getSnum());
        int count = stmt.executeUpdate();
        return count;
    }

    /**
     * @description: 删除一门课程
     * @param {String} cid 课程编号
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */   
    @Override  
    public int deleteOneCourse(String cid) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "delete from course where id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, cid);
        int count = stmt.executeUpdate();
        return count;
    }

    @Override
    /**
     * @description: 查询所有课程
     * @return {*} 课程集合
     * @author: 姜东浩
     */    
    public List<Course> selectAllCourses() throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "select id,cname,num,snum from course";
        PreparedStatement stmt = conn.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();
        List<Course> courseList = new ArrayList<Course>();
        while (rs.next()) {
            Course course = new Course();
            course.setId(rs.getString("id"));
            course.setCname(rs.getString("cname"));
            course.setNum(rs.getInt("num"));
            course.setSnum(rs.getInt("snum"));
            courseList.add(course);
        }
        return courseList;
    }
    
    // =========================================================================================================
   
    /**
     * @description: 添加一名学生
     * @param {Student} student
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */
    @Override
    public int insertOneStudent(Student student) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "insert into student(id,sname,pwd,sex) values(?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, student.getStudentId());
        stmt.setString(2, student.getSname());
        stmt.setString(3, MD5.md5(student.getPassword()));
        stmt.setString(4, student.getSex());
        int count = stmt.executeUpdate();
        return count;
    }

    /**
     * @description: 删除一名学生
     * @param {String} sid 学生编号
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */   
    @Override
    public int deleteOneStudent(String sid) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "delete from student where id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, sid);
        int count = stmt.executeUpdate();
        return count;
    }

    /**
     * @description: 修改学生密码
     * @param {String} sid 学生编号
     * @param {String} pwd 新密码
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */    
    @Override
    public int updateStudentPwd(String sid, String pwd) throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "update student set pwd =? where id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, MD5.md5(pwd));
        stmt.setString(2, sid);
        int count = stmt.executeUpdate();
        return count;
    }

     /**
     * @description: 查询所有学生
     * @param {*}
     * @return {*} 学生集合
     * @author: 姜东浩
     */    
    @Override
    public List<Student> selectALlStudents() throws SQLException {
        Connection conn = JDBCUtil.getConn();
        String sql = "select id,sname,sex from student";
        PreparedStatement stmt = conn.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();
        List<Student> studentList = new ArrayList<Student>();
        while (rs.next()) {
            Student student = new Student();
            student.setStudentId(rs.getString("id"));
            student.setSname(rs.getString("sname"));
            student.setSex(rs.getString("sex"));
            studentList.add(student);
        }
        return studentList;
    }

}