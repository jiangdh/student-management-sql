package com.nichol.dao;

import java.sql.SQLException;
import java.util.List;

import com.nichol.pojo.Course;
import com.nichol.pojo.Student;

/*
 * @Author: 姜东浩
 * @Date: 2021-12-01 19:48:43
 * @Description: 学生数据库接口类
 */
public interface StudentDAO {

    /**
     * @description: 学生登录
     * @param {Student} student 学生对象
     * @return {*} 登陆对象
     * @author: 姜东浩
     */    
    public abstract Student login(Student student) throws SQLException;

    /**
     * @description: 学生选课
     * @param {String} cid 课程编号
     * @return {*}
     * @author: 姜东浩
     */    
    public abstract int chooseCourse(String cid) throws SQLException;
  
    /**
     * @description: 删除选课
     * @param {String} cid 课程编号
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */      
    public abstract int deleteCourse(String cid) throws SQLException;

    /**
     * @description: 查看已选课程
     * @return {*}课程集合
     * @author: 姜东浩
     */      
    public abstract List<Course> selectAllChooseCourse() throws SQLException;

    /**
     * @description: 查看所有课程
     * @param {*}
     * @return {*} 课程集合
     * @author: 姜东浩
     */    
    public abstract List<Course> selectAllCourse() throws SQLException; 

    /**
     * @description: 选课人数操作
     * @param {String} oper 表示人数增加还是减少
     * @param {String} cid 课程编号
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */    
    public abstract int updateCourseNum(String oper,String cid) throws SQLException; 

    /**
     * @description: 修改密码
     * @param {String} pwd 新密码
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */    
    public abstract int updatePassword(String pwd) throws SQLException;

    /**
     * @description: 根据编号查找课程
     * @param {String} cid
     * @return {*} 该课程
     * @author: 姜东浩
     */    
    public abstract Course selectCourseById(String cid) throws SQLException; 
    
}