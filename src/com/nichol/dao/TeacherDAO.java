package com.nichol.dao;

import java.sql.SQLException;
import java.util.List;

import com.nichol.pojo.Course;
import com.nichol.pojo.Student;
import com.nichol.pojo.Teacher;

/*
 * @Author: 姜东浩
 * @Date: 2021-12-01 23:18:06
 * @Description: 教师数据操作接口
 */
public interface TeacherDAO {

    /**
     * @description: 教师登录
     * @param {Teacher} teacher
     * @return {*} 登录教师对象
     * @author: 姜东浩
     */
    public abstract Teacher login(Teacher teacher) throws SQLException;

    /**
     * @description: 登分
     * @param {String} sid
     * @param {String} cid
     * @param {double} grade
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */
    public abstract int insertStudentGrade(String sid, double grade) throws SQLException;

    /**
     * @description: 改分
     * @param {String} sid
     * @param {String} cid
     * @param {double} grade
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */
    public abstract int updateStudentGrade(String sid, double grade) throws SQLException;

    /**
     * @description: 查看自己的学生列表
     * @return {*} 学生集合
     * @author: 姜东浩
     */
    public abstract List<Student> selectStudentList() throws SQLException;

    /**
     * @description: 查看自己所授课程
     * @return {*} 课程对象
     * @author: 姜东浩
     */
    public abstract Course selectMyCourse() throws SQLException;

    /**
     * @description: 修改密码
     * @param {String} pwd
     * @return {*} 数据库影响行数
     * @author: 姜东浩
     */
    public abstract int updatePassword(String pwd) throws SQLException;

}