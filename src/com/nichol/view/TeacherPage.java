package com.nichol.view;

import java.util.Scanner;

import com.nichol.controller.TeacherController;
import com.nichol.util.InputTrans;

/*
 * @Author: 姜东浩
 * @Date: 2021-12-01 22:45:47
 * @Description: 教师登录后页面
 */
public class TeacherPage {

    static Scanner sc = new Scanner(System.in);
    static TeacherController teacherController = new TeacherController();

    /**
     * @description: 教师登陆成功页面
     * @author: 姜东浩
     */
    public static void showLoginSuccess() {
        do {
            System.out.println("**************************");
            System.out.println("* 1、登分                 *");
            System.out.println("* 2、改分                 *");
            System.out.println("* 3、查看学生列表          *");
            System.out.println("* 4、查看自己所教课程       *");
            System.out.println("* 5、修改密码              *");
            System.out.println("* 6、退出程序              *");
            System.out.println("**************************");

            switch (InputTrans.betweenStatedInt(sc.next())) {
                case 1: // 登分
                    System.out.println(teacherController.insertStudentGrade());
                    break;
                case 2: // 改分
                    System.out.println(teacherController.updateStudentGrade());
                    break;
                case 3: // 查看学生列表
                    teacherController.selectStudentList();
                    break;
                case 4: // 查看自己所教课程
                    teacherController.selectMyCourse();
                    break;
                case 5: // 修改密码
                    System.out.println(teacherController.updatePassword());
                    break;
                case 6: // 退出程序
                    System.out.println("感谢您的使用,再见!");
                    System.exit(0);
                default: // 输入不正确重新输入
                    System.out.println("ERROR: 您的输入有误，请重新选择功能!");
                    break;
            }
        } while (true);

    }

}