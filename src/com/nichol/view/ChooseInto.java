package com.nichol.view;

import com.nichol.controller.AdminController;
import com.nichol.controller.StudentController;
import com.nichol.controller.TeacherController;

/*
 * @Author: 姜东浩
 * @Date: 2021-11-10 11:18:28
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-12-02 08:50:06
 * @Description: 判断用户类型，并选择其所对应的登录判断
 */
public class ChooseInto {

    static TeacherController teacherController = new TeacherController();
    static AdminController adminController = new AdminController();
    static StudentController studentController = new StudentController();

    /**
     * @description:判断登录用户的类型，跳转相应的登录判断器
     * @param sign     用户类型
     * @param userName 账号
     * @param password 密码
     * @author: 姜东浩
     */
    public static void gotoPage(String sign, String userName, String password) {
        switch (sign) {
            case "a": // 管理员登录
                adminController.login(userName, password);
                break;
            case "t": // 教师登录
                teacherController.login(userName, password);
                break;
            case "s": // 学生登录
                studentController.login(userName, password);
                break;
            default: // 账号输入有问题
                System.out.println("ERROE : 账号输入有误，请检查后重新输入!");
                break;
        }
    }
}