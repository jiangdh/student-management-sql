package com.nichol.view;

import java.util.Scanner;

import com.nichol.util.InputTrans;
import com.nichol.controller.AdminController;

/*
 * @Author: 姜东浩
 * @Date: 2021-12-01 17:39:38
 * @Description: 超级管理员登录显示页面元素
 */
public class AdminPage {

    static Scanner sc = new Scanner(System.in);
    static AdminController adminController = new AdminController();

    /**
     * @description: 超级管理员登陆后展示的首页
     * @author: 姜东浩
     */
    public static void showLoginSuccess() {
        do {
            System.out.println("**************************");
            System.out.println("* 1、学生管理              *");
            System.out.println("* 2、教师管理              *");
            System.out.println("* 3、课程管理              *");
            System.out.println("* 4、修改密码              *");
            System.out.println("* 5、退出程序              *");
            System.out.println("**************************");

            switch (InputTrans.betweenStatedInt(sc.next())) {
                case 1: // 学生管理
                    showOperStudent();
                    break;
                case 2: // 教师管理
                    showOperTeacher();
                    break;
                case 3: // 课程管理
                    showOperCourse();
                    break;
                case 4: // 修改密码
                    adminController.updatePwd();
                    break;
                case 5: // 退出程序
                    System.out.println("感谢您的使用,再见!");
                    System.exit(0);
                default: // 输入不正确重新输入
                    System.out.println("ERROR: 您的输入有误，请重新选择功能!");
                    break;
            }
        } while (true);

    }

    /**
     * @description: 学生管理页面
     * @author: 姜东浩
     */
    private static void showOperStudent() {

        boolean flag = true;
        do {
            System.out.println("***************************");
            System.out.println("*1、生成学生账号            *");
            System.out.println("*2、删除学生账号            *");
            System.out.println("*3、修改学生密码            *");
            System.out.println("*4、查看所有学生            *");
            System.out.println("*5、返回上一级              *");
            System.out.println("***************************");

            switch (InputTrans.betweenStatedInt(sc.next())) {
                case 1: // 生成学生账号
                    System.out.println(adminController.insertOneStudent());
                    break;
                case 2: // 删除学生账号
                    System.out.println(adminController.deleteOneStudent());
                    break;
                case 3: // 修改学生密码
                    System.out.println(adminController.updateStudentPwd());
                    break;
                case 4: // 查看所有学生
                    adminController.selectALlStudents();
                    break;
                case 5: // 返回上一级
                    flag = false;
                    break;
                default: // 输入不正确重新输入
                    System.out.println("ERROR: 您的输入有误，请重新选择功能!");
                    break;
            }
        } while (flag);

    }

    /**
     * @description: 教师管理页面
     * @author: 姜东浩
     */
    private static void showOperTeacher() {

        boolean flag = true;
        do {
            System.out.println("****************************");
            System.out.println("*1、生成教师账号              *");
            System.out.println("*2、删除教师账号              *");
            System.out.println("*3、修改教师密码              *");
            System.out.println("*4、设置教师所授科目           *");
            System.out.println("*5、查看所有教师              *");
            System.out.println("*6、返回上一级                *");
            System.out.println("****************************");

            switch (InputTrans.betweenStatedInt(sc.next())) {
                case 1: // 生成教师账号
                    System.out.println(adminController.insertOneTeacher());
                    break;
                case 2: // 删除教师账号
                    System.out.println(adminController.deleteOneTeacher());
                    break;
                case 3: // 修改教师密码
                    System.out.println(adminController.updateTeacherPwd());
                    break;
                case 4: // 设置教师所授科目
                    System.out.println(adminController.updateTeacherCid());
                    break;
                case 5: // 查看所有教师
                    adminController.selectAllTeachers();
                    break;
                case 6: // 返回上一级
                    flag = false;
                    break;
                default: // 输入不正确重新输入
                    System.out.println("ERROR: 输入不正确重新输入!");
                    break;
            }
        } while (flag);

    }

    /**
     * @description: 课程管理页面
     * @author: 姜东浩
     */
    private static void showOperCourse() {

        boolean flag = true;
        do {
            System.out.println("**************************");
            System.out.println("*1、新增课程               *");
            System.out.println("*2、删除课程               *");
            System.out.println("*3、查看所有课程            *");
            System.out.println("*4、返回上一级              *");
            System.out.println("**************************");

            switch (InputTrans.betweenStatedInt(sc.next())) {
                case 1: // 新增课程
                    System.out.println(adminController.insertOneCourse());
                    break;
                case 2: // 删除课程
                    System.out.println(adminController.deleteOneCourse());
                    break;
                case 3: // 查看所有课程
                    adminController.selectAllCourses();
                    break;
                case 4:// 返回上一级
                    flag = false;
                    break;
                default: // 输入不正确重新输入
                    System.out.println("ERROR: 您的输入有误，请重新选择功能!");
                    break;
            }
        } while (flag);

    }

}