package com.nichol.view;

import java.util.Scanner;

import com.nichol.controller.StudentController;
import com.nichol.util.InputTrans;

/*
 * @Author: 姜东浩
 * @Date: 2021-11-30 14:39:09
 * @Description: 学生登陆成功后显示页面元素
 */
public class StudentPage {
    static Scanner sc = new Scanner(System.in);
    static StudentController studentController = new StudentController();

    /**
     * @description: 学生登陆后展示的首页
     * @author: 姜东浩
     */
    public static void showLoginSuccess() {
        do {
            System.out.println("**************************");
            System.out.println("* 1、学生选课              *");
            System.out.println("* 2、删除选课              *");
            System.out.println("* 3、查看已选课程           *");
            System.out.println("* 4、修改密码              *");
            System.out.println("* 5、退出程序              *");
            System.out.println("**************************");

            switch (InputTrans.betweenStatedInt(sc.next())) {
                case 1: // 学生选课
                    System.out.println(studentController.chooseCourse());
                    break;
                case 2: // 删除选课
                    System.out.println(studentController.deleteCourse());
                    break;
                case 3: // 查看已选课程
                    studentController.selectAllChooseCourse();
                    break;
                case 4: // 修改密码
                    System.out.println(studentController.updatePassword());
                    break;
                case 5: // 退出程序
                    System.out.println("感谢您的使用,再见!");
                    System.exit(0);
                default: // 输入不正确重新输入
                    System.out.println("ERROR: 您的输入有误，请重新选择功能!");
                    break;
            }
        } while (true);

    }

}