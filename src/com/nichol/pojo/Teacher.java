package com.nichol.pojo;

/*
 * @Author: 姜东浩
 * @Date: 2021-12-01 10:07:42
 * @Description: 教师类
 */
public class Teacher {
    
    // 教师ID，用来登录系统
    private String teacherId;
    // 教师姓名
    private String teacherName;
    // 教师密码
    private String password;
    // 教师性别
    private String sex;
    // 教师教的科目编号
    private String cId;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getcId() {
        return cId;
    }

    public void setcId(String cId) {
        this.cId = cId;
    }

    public Teacher() {
    }

    public Teacher(String teacherId, String password) {
        this.teacherId = teacherId;
        this.password = password;
    }

    public Teacher(String teacherId, String teacherName, String password, String sex) {
        this.teacherId = teacherId;
        this.teacherName = teacherName;
        this.password = password;
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Teacher [cId=" + cId + ", password=" + password + ", sex=" + sex + ", teacherId=" + teacherId
                + ", teacherName=" + teacherName + "]";
    }

}