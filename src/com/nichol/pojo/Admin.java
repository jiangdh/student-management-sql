package com.nichol.pojo;

/*
 * @Author: 姜东浩
 * @Date: 2021-11-30 19:35:45
 * @Description: 管理员实体类
 */
public class Admin {
    
    //用户名
    private String adminId;
    //密码
    private String password;

    public String getAdminId() {
        return adminId;
    }
    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
    public Admin() {
    }
    
    public Admin(String adminId, String password) {
        this.adminId = adminId;
        this.password = password;
    }

    @Override
    public String toString() {
        return "Admin [adminId=" + adminId + ", password=" + password + "]";
    }
     
}