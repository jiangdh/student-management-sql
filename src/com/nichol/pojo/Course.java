package com.nichol.pojo;

/*
 * @Author: 姜东浩
 * @Date: 2021-12-01 16:12:51
 * @Description: 课程类
 */
public class Course {

    // 课程编号
    private String id;
    // 课程名
    private String cname;
    // 课程已选人数
    private int num;
    // 课程容量
    private int snum;
    // 成绩
    private double grade;

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getSnum() {
        return snum;
    }

    public void setSnum(int snum) {
        this.snum = snum;
    }

    public Course() {
    }

    public Course(String id, String cname, int snum) {
        this.id = id;
        this.cname = cname;
        this.snum = snum;
    }
    
    @Override
    public String toString() {
        return "Course [cname=" + cname + ", id=" + id + ", num=" + num + ", snum=" + snum + "]";
    }

}