package com.nichol.pojo;

/*
 * @Author: 姜东浩
 * @Date: 2021-12-01 17:01:39
 * @Description: 学生实体类
 */
public class Student {

    // 学生编号
    private String studentId;
    // 学生姓名
    private String sname;
    // 学生密码
    private String password;
    // 学生性别
    private String sex;
    // 学生分数
    private double grade;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public Student() {
    }

    public Student(String studentId, String password) {
        this.studentId = studentId;
        this.password = password;
    }

    public Student(String studentId, String sname, String password, String sex) {
        this.studentId = studentId;
        this.sname = sname;
        this.password = password;
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Student [password=" + password + ", sex=" + sex + ", sname=" + sname + ", studentId=" + studentId + "]";
    }

}