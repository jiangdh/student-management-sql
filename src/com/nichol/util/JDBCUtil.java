package com.nichol.util;

import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;
import java.sql.SQLException;
import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import org.apache.commons.dbcp.BasicDataSourceFactory;

/*
 * @Author: 姜东浩
 * @Date: 2021-11-30 19:17:02
 * @Description: JDBC数据库连接,Druid数据库连接池
 */
public class JDBCUtil {

    private static ThreadLocal<Connection> threadLocal = new ThreadLocal<>();
    private static DataSource ds = null;
    
    static{
        try{
            Properties properties = new Properties();
            InputStream is = JDBCUtil.class.getResourceAsStream("../resources/db.properties");
            properties.load(is);
            // ds = BasicDataSourceFactory.createDataSource(properties);
            ds = DruidDataSourceFactory.createDataSource(properties);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * @description: 获取连接
     * @param {*}
     * @return conn 数据库连接对象
     * @author: 姜东浩
     */    
    public static Connection getConn() {
        Connection conn = threadLocal.get();
        try {
            if(conn==null||conn.isClosed()){
                conn = ds.getConnection();
                threadLocal.set(conn);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * @description: 关闭连接
     * @param {*}
     * @return void
     * @author: 姜东浩
     */    
    public static void closeConn(){
        Connection conn = threadLocal.get();
        try{
            if(conn!=null&&!conn.isClosed()){
                conn.close();
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            threadLocal.set(null);
        }
    }

} 