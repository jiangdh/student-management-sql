package com.nichol.util;

import java.util.Vector;

/*
 * @Author: 姜东浩
 * @Date: 2021-11-30 22:10:25
 * @Description: 将用户的输入转换为所要的类型返回，多用于页面功能选择
 */
public class InputTrans {

    /**
     * @description:将用户输入的字符串转（纯数字）转换为int类型数字，转换失败返回-1
     * @param s 用户输入的字符串
     * @return 转换后的int型数字或-1
     * @author: 姜东浩
     */
    public static int transitionToInt(String s) {

        try {
            int num = Integer.parseInt(s);
            return num;
        } catch (Exception e) {
            return -1;
        }

    }

    /**
     * @description:将用户输入的字符串转（纯数字）转换为long类型，转换失败返回-1
     * @param s 用户输入的字符串转
     * @return 转换后的long型数字或-1
     * @author: 姜东浩
     */
    public static long transitionToLong(String s) {

        try {
            long num = Long.parseLong(s);
            return num;
        } catch (Exception e) {
            return -1;
        }

    }

    /**
     * @description:将用户输入的字符串转（纯数字）转换为double类型数字，转换失败返回-1
     * @param s 用户输入的字符串
     * @return 转换后的double型数字或-1
     * @author: 姜东浩
     */
    public static double transitionToDouble(String s) {

        try {
            double num = Double.parseDouble(s);
            return num;
        } catch (Exception e) {
            return -1;
        }

    }

    /**
     * @description:用于选择功能是的判断，成功返回转换后的int类型，转换失败返回-1
     * @param s 用户输入的字符串
     * @return 转换后的int型数字或-1
     * @author: 姜东浩
     */
    public static int betweenStatedInt(String s) {

        // 声明一个向量，用来存储当用户进行菜单选择功能时可以输入值的范围
        Vector<Integer> v = new Vector<Integer>();

        // 循环添加用户可以输入的值
        for (int i = 0; i < 10; i++) {
            v.addElement(i + 1);
        }

        // 将用户输入转换为int类型
        // 将转换后的int值减一作为向量索引
        // 在向量中查找，找到后说明可以输入，返回处理后用户输入的值，报错则说明不能输入该值返回-1
        try {
            if (v.get(Integer.parseInt(s) - 1) > 0) {
                return Integer.parseInt(s);
            } else {
                return -1;
            }
        } catch (Exception e) {
            return -1;
        }

    }

    /**
     * @description:用于对输入账号类型的判读，判断开头是否为[r,t]中的一个,或者是纯数字，并且它们后面是纯数字&&纯数字不能全为0
     * @param s 用户输入的字符串转
     * @return 判断后给定的标志符
     * @author: 姜东浩
     */
    public static String judgeTA(String s) {

        // 从第二个到最后一个字符串
        long num = 0;

        // 如果转化为long类型成功，说明是纯数字，那就是学生账号
        try {
            if (Long.parseLong(s) > 0) {
                return "s";
            }
        } catch (Exception e) {
        }

        if (s.startsWith("a") || s.startsWith("t")) {
            try {
                num = Long.parseLong(s.substring(1, s.length()));
                if (num > 0) {
                    return s.charAt(0) + "";
                } else {
                    return "null";
                }
            } catch (Exception ex) {
                return "null";
            }

        } else {
            return "null";
        }

    }

    /**
     * @description:用于对课程编号的判读判断开头是否为[b,x]中的一个,或者是纯数字，并且它们后面是纯数字&&纯数字不能全为0
     * @param s 用户输入的字符串
     * @return 判断后给定的标志符
     * @author: 姜东浩
     */
    public static String judgeBX(String s){

        // 从第二个到最后一个字符串
        long num = 0;
        if (s.startsWith("b") || s.startsWith("x")) {
            try {
                num = Long.parseLong(s.substring(1, s.length()));
                if (num > 0) {
                    return s.charAt(0) + "";
                } else {
                    return "null";
                }
            } catch (Exception ex) {
                return "null";
            }

        } else {
            return "null";
        }

    }
    
    /**
     * @description: 用于判断性别输入是否正确
     * @param s 用户输入的字符串
     * @return true/false
     * @author: 姜东浩
     */
    public static boolean judgeWM(String s){
        if(s.length()!=1){
            return false;
        }
        if (s.toUpperCase().startsWith("M") || s.toUpperCase().startsWith("W")) {
            return true;
        } else {
            return false;
        }
    }

}
