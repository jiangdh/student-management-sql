package com.nichol.util;

import java.security.MessageDigest;

/*
 * @Date: 2021-11-30 19:13:35
 * @Description: 工具类，用来加密密码
 */
public class MD5 {
     // 盐，用于混交md5
     private static final String slat = "&%1A2Asc*&%$$#@";

     /**
      * @description:原生的加密方法
      * @param value 需要加密的字符串
      * @return 加密后的字符串
      */
     public static String md5(String value) {
         
         try {
             value = value + slat;
             MessageDigest messageDigest = MessageDigest.getInstance("MD5");
             messageDigest.update(value.getBytes("UTF8"));
             byte message[] = messageDigest.digest();
             String result = "";
             for (int i = 0; i < message.length; i++) {
                 result += Integer.toHexString((0x000000FF & message[i]) | 0xFFFFFF00).substring(6);
             }
             return result;
         } catch (Exception e) {
             e.printStackTrace();
         }
         return null;
         
     }
}