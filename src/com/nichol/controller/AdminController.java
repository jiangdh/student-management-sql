package com.nichol.controller;

import java.util.List;
import java.util.Scanner;

import com.nichol.pojo.Admin;
import com.nichol.pojo.Course;
import com.nichol.pojo.Student;
import com.nichol.pojo.Teacher;
import com.nichol.service.AdminService;
import com.nichol.service.impl.AdminServiceImpl;
import com.nichol.util.InputTrans;
import com.nichol.view.AdminPage;

/*
 * @Author: 姜东浩
 * @Date: 2021-11-10 6:50:04
 * @Description: 管理员控制器
 */
public class AdminController {

    Scanner sc = new Scanner(System.in);
    AdminService adminService = new AdminServiceImpl();

    // =========================================================================================================

    /**
     * @description:登录
     * @param adminId  管理员ID
     * @param password 管理员密码
     * @author: 姜东浩
     */
    public void login(String adminId, String password) {

        // 将传进来的账号密码封装为一个管理员对象
        Admin admin = new Admin(adminId, password);

        // 登录成功显示首页视图，否则提示登陆失败
        if (adminService.login(admin) != null) {
            AdminPage.showLoginSuccess();
        } else {
            System.out.println("ERROR : 登录失败!");
        }

    }

    /**
     * @description: 修改管理员密码
     * @author: 姜东浩
     */
    public void updatePwd() {

        System.out.print("请输入修改后的密码：");
        if (adminService.updatePwd(sc.next()) == 1) {
            System.out.println("SUCCESS : 修改管理员密码成功,请重新登陆!");
            // 修改成功后，系统退出，需要重新登陆
            System.exit(0);
        } else {
            System.out.println("ERROE : 修改管理员密码失败!");
        }

    }

    // =========================================================================================================

    /**
     * @description: 添加一名教师
     * @return 添加操作后的提示信息
     * @author: 姜东浩
     */
    public String insertOneTeacher() {

        System.out.print("请输入教师账号：");
        // 去除用户输入字符串两端的空格
        String teacherId = sc.next().trim();
        if (!("t".equals(InputTrans.judgeTA(teacherId)))) {
            return "ERROR: ID类型与规定的教师类型不符";
        }

        System.out.print("请输入教师姓名：");
        // 去除用户输入字符串两端的空格
        String teacherName = sc.next().trim();

        System.out.print("请输入教师性别（M/W）：");
        // 去除用户输入字符串两端的空格
        String teacherSex = sc.next().trim();
        if (!InputTrans.judgeWM(teacherSex)) {
            return "ERROR: 性别类型与规定的类型不符";
        }
        String ts = teacherSex.toUpperCase().equals("M") ? "男" : "女";

        // 将用户输入封装为一位教师对象，初始密码为123456
        Teacher teacher = new Teacher(teacherId, teacherName, "123456", ts);

        if (adminService.insertOneTeacher(teacher) == 1) {
            return "SUCCESS: 添加成功!";
        } else {
            return "ERROR: ID已存在,添加失败!";
        }

    }

    /**
     * @description: 删除一位教师
     * @return 删除操作结束后返回的提示信息
     * @author: 姜东浩
     */
    public String deleteOneTeacher() {

        System.out.print("请输入教师账号：");
        // 去除用户输入字符串两端的空格
        String teacherId = sc.next().trim();
        if (!("t".equals(InputTrans.judgeTA(teacherId)))) {
            return "ERROR: ID类型与规定的教师类型不符";
        }

        // 进行删除操作
        if (adminService.deleteOneTeacher(teacherId) == 1) {
            return "SUCCESS: 删除成功!";
        } else {
            return "ERROR: ID不存在,删除失败!";
        }

    }

    /**
     * @description: 修改教师密码
     * @return 修改操作结束后的提示信息
     * @author: 姜东浩
     */
    public String updateTeacherPwd() {

        System.out.print("请输入教师账号：");
        // 去除用户输入字符串两端的空格
        String teacherId = sc.next().trim();
        if (!("t".equals(InputTrans.judgeTA(teacherId)))) {
            return "ERROR: ID类型与规定的教师类型不符";
        }

        System.out.print("请输入修改后的密码：");
        // 修改密码
        if (adminService.updateTeacherPwd(teacherId, sc.next()) == 1) {
            return "SUCCESS: 修改教师密码成功!";
        } else {
            return "ERROR: ID不存在,修改教师密码失败!";
        }

    }

    /**
     * @description: 设置老师所授课程
     * @return 返回操作后的提示
     * @author: 姜东浩
     */
    public String updateTeacherCid() {

        System.out.print("请输入教师账号：");
        // 去除用户输入字符串两端的空格
        String teacherId = sc.next().trim();
        if (!("t".equals(InputTrans.judgeTA(teacherId)))) {
            return "ERROR: ID类型与规定的教师类型不符";
        }

        // 查看文件中是否有此ID教师
        if (adminService.selectTeacher(teacherId) == null) {
            return "ERROR: 没有此ID教师!";
        }

        System.out.print("请输入课程ID：");
        // 去除用户输入字符串两端的空格
        String courseId = sc.next().trim();
        if (!("b".equals(InputTrans.judgeBX(courseId)) || "x".equals(InputTrans.judgeBX(courseId)))) {
            return "ERROR: ID类型与规定的课程类型不符";
        }

        if (adminService.updateTeacherCid(teacherId, courseId) == 1) {
            return "SUCCESS: 设置教师所授课程成功!";
        } else {
            return "ERROR: 设置教师所授课程失败!";
        }

    }

    /**
     * @description: 询所有教师
     * @param {*}
     * @return {*}
     * @author: 姜东浩
     */
    public void selectAllTeachers() {
        List<Teacher> teacherList = adminService.selectAllTeachers();
        for (int i = 0; i < teacherList.size(); i++) {
            String tc = teacherList.get(i).getcId() == null ? "无在授课程" : teacherList.get(i).getcId();
            System.out.println(
                    "教师编号:" + teacherList.get(i).getTeacherId() + "  教师姓名:" + teacherList.get(i).getTeacherName()
                            + "  教师性别:" + teacherList.get(i).getSex() + "  教师所授课程编号:" + tc);
        }
    }

    // =========================================================================================================

    /**
     * @description: 添加一门课程
     * @return {String} 返回添加后提示信息
     * @author: 姜东浩
     */
    public String insertOneCourse() {

        System.out.print("请输入课程ID：");
        // 去除用户输入字符串两端的空格
        String courseId = sc.next().trim();
        if (!("b".equals(InputTrans.judgeBX(courseId)) || "x".equals(InputTrans.judgeBX(courseId)))) {
            return "ERROR: ID类型与规定的课程类型不符!";
        }

        System.out.print("请输入课程名：");
        // 去除用户输入字符串两端的空格
        String courseName = sc.next().trim();

        System.out.print("请输入课程可报人数（int）");
        int snum = InputTrans.transitionToInt(sc.next());
        if (snum == -1) {
            return "ERROR: 人数输入有误!";
        }

        Course course = new Course(courseId, courseName, snum);

        if (adminService.insertOneCourse(course) == 1) {
            return "SUCCESS: 添加成功!";
        } else {
            return "ERROR: ID已存在,添加失败!";
        }

    }

    /**
     * @description: 删除一门课程
     * @return {String} 返回添加后提示信息
     * @author: 姜东浩
     */
    public String deleteOneCourse() {

        System.out.print("请输入课程ID：");
        // 去除用户输入字符串两端的空格
        String courseId = sc.next().trim();
        if (!("b".equals(InputTrans.judgeBX(courseId)) || "x".equals(InputTrans.judgeBX(courseId)))) {
            return "ERROR: ID类型与规定的课程类型不符!";
        }

        if (adminService.deleteOneCourse(courseId) == 1) {
            return "SUCCESS: 删除成功!";
        } else {
            return "ERROR: 删除失败!";
        }

    }

    /**
     * @description: 询所有课程
     * @param {*}
     * @return {*}
     * @author: 姜东浩
     */
    public void selectAllCourses() {
        List<Course> courseList = adminService.selectAllCourses();
        for (int i = 0; i < courseList.size(); i++) {
            System.out.println(
                    "课程编号:" + courseList.get(i).getId() + "  课程名称:" + courseList.get(i).getCname()
                            + "  已报人数:" + courseList.get(i).getNum() + "  总容量:" + courseList.get(i).getSnum());
        }
    }

    // =========================================================================================================

    /**
     * @description: 添加一名学生
     * @return 添加操作结束后的提示信息
     * @author: 姜东浩
     */
    public String insertOneStudent() {

        System.out.print("请输入学生账号（纯数字）：");
        // 去除用户输入字符串两端的空格
        String studentId = sc.next().trim();
        if (!("s".equals(InputTrans.judgeTA(studentId)))) {
            return "ERROR: 学号类型与规定的学生学号类型不符!";
        }

        System.out.print("请输入学生姓名：");
        // 去除用户输入字符串两端的空格
        String studentName = sc.next().trim();

        System.out.print("请输入学生性别（M/W）：");
        // 去除用户输入字符串两端的空格
        String studentSex = sc.next().trim();
        if (!InputTrans.judgeWM(studentSex)) {
            return "ERROR: 性别类型与规定的类型不符!";
        }
        String ss = studentSex.toUpperCase().equals("M") ? "男" : "女";

        // 将用户输入封装为一个学生对象，密码默认为123456
        Student student = new Student(studentId, studentName, "123456", ss);

        // 如果没有此学号的学生，则进行添加操作，否则添加失败，返回失败信息
        if (adminService.insertOneStudent(student) == 1) {
            return "SUCCESS: 添加成功!";
        } else {
            return "ERROR: ID已存在,添加失败!";
        }

    }

    /**
     * @description: 删除一名学生
     * @return 删除操作结束后的提示信息
     * @author: 姜东浩
     */
    public String deleteOneStudent() {

        System.out.print("请输入学生账号：");
        // 去除用户输入字符串两端的空格
        String studentId = sc.next().trim();
        if (!("s".equals(InputTrans.judgeTA(studentId)))) {
            return "学号类型与规定的学生学号类型不符";
        }

        // 进行删除操作
        if (adminService.deleteOneStudent(studentId) == 1) {
            return "SUCCESS: 删除成功!";
        } else {
            return "ERROR: ID不存在,删除失败!";
        }

    }

    /**
     * @description: 修改学生密码
     * @return 修改操作结束后的提示信息
     * @author: 姜东浩
     */
    public String updateStudentPwd() {

        System.out.print("请输入学生账号：");
        // 去除用户输入字符串两端的空格
        String studentId = sc.next().trim();
        if (!("s".equals(InputTrans.judgeTA(studentId)))) {
            return "ERROR: ID类型与规定的学生类型不符";
        }

        System.out.print("请输入修改后的密码：");
        // 修改密码
        if (adminService.updateStudentPwd(studentId, sc.next()) == 1) {
            return "SUCCESS: 修改学生密码成功!";
        } else {
            return "ERROR: ID不存在,修改学生密码失败!";
        }

    }

    /**
     * @description: 询所有学生
     * @param {*}
     * @return {*}
     * @author: 姜东浩
     */
    public void selectALlStudents() {
        List<Student> studentList = adminService.selectALlStudents();
        for (int i = 0; i < studentList.size(); i++) {
            System.out.println(
                    "学生编号:" + studentList.get(i).getStudentId() + "  学生姓名:" + studentList.get(i).getSname()
                            + "  学生性别:" + studentList.get(i).getSex());
        }
    }

}