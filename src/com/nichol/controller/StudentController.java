package com.nichol.controller;

import java.util.List;
import java.util.Scanner;

import com.nichol.pojo.Course;
import com.nichol.pojo.Student;
import com.nichol.service.StudentService;
import com.nichol.service.impl.StudentServiceImpl;
import com.nichol.view.StudentPage;

/*
 * @Author: 姜东浩
 * @Date: 2021-12-01 19:30:25
 * @Description: 学生控制器类
 */
public class StudentController {

    Scanner sc = new Scanner(System.in);
    StudentService studentService = new StudentServiceImpl();

    /**
     * @description: 学生登录
     * @param {String} id
     * @param {String} password
     * @return {*}
     * @author: 姜东浩
     */
    public void login(String id, String password) {
        // 将传进来的账号密码封装为一个管理员对象
        Student student = new Student(id, password);

        // 登录成功显示首页视图，否则提示登陆失败
        if (studentService.login(student) != null) {
            StudentPage.showLoginSuccess();
        } else {
            System.out.println("ERROR : 登录失败!");
        }
    }

    /**
     * @description: 学生选课
     * @param {*}
     * @return {*} 操作提示信息
     * @author: 姜东浩
     */
    public String chooseCourse() {
        List<Course> courseList = studentService.selectAllCourse();
        for (int i = 0; i < courseList.size(); i++) {
            int num = courseList.get(i).getNum();
            int snum = courseList.get(i).getSnum();
            String cid = courseList.get(i).getId();
            String cname = courseList.get(i).getCname();
            String state = snum > num ? "可选" : "不可选";
            System.out
                    .println("课程编号:" + cid + "  课程名:" + cname + "  已选人数:" + num + "  课程容量:" + snum + "  课程状态:" + state);
        }

        System.out.println("====退出请输入：exit");
        System.out.print("请输入要选择的课程号：");
        String usc = sc.next();
        if (usc.equals("exit")) {
            return "";
        }

        if (studentService.chooseCourse(usc) == 1) {
            return "SUCCESS: 选课成功!";
        } else {
            return "ERROR: 选课失败!";
        }

    }

    /**
     * @description: 删除课程
     * @param {*}
     * @return {*} 操作提示信息
     * @author: 姜东浩
     */    
    public String deleteCourse() {
        List<Course> courseList = studentService.selectAllChooseCourse();
        for (int i = 0; i < courseList.size(); i++) {
            String cid = courseList.get(i).getId();
            String cname = courseList.get(i).getCname();
            double grade = courseList.get(i).getGrade();
            System.out.println("课程编号:" + cid + "  课程名称:" + cname + "  成绩:" + grade);
        }
        System.out.println("====退出请输入：exit");
        System.out.print("请输入要删除的课程号：");
        String usc = sc.next();
        if (usc.equals("exit")) {
            return "";
        }
        if (studentService.deleteCourse(usc) == 1) {
            return "SUCCESS: 删除课程成功!";
        } else {
            return "ERROR: 删除课程失败!";
        }

    }
    
    /**
     * @description: 查看已选课程
     * @param {*}
     * @return {*}
     * @author: 姜东浩
     */
    public void selectAllChooseCourse(){
        List<Course> courseList = studentService.selectAllChooseCourse();
        for (int i = 0; i < courseList.size(); i++) {
            String cid = courseList.get(i).getId();
            String cname = courseList.get(i).getCname();
            double grade = courseList.get(i).getGrade();
            System.out.println("课程编号:" + cid + "  课程名称:" + cname + "  成绩:" + grade);
        }
    }

    /**
     * @description: 修改学生密码
     * @param {*}
     * @return {*} 修改操作结束后的提示信息
     * @author: 姜东浩
     */    
    public String updatePassword(){
        System.out.print("请输入修改后的密码：");
        // 修改密码
        if (studentService.updatePassword(sc.next()) == 1) {
            return "SUCCESS: 修改密码成功!";
        } else {
            return "ERROR: ID不存在,修改密码失败!";
        }
    }

}