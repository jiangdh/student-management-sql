package com.nichol.controller;

import java.util.List;
import java.util.Scanner;

import com.nichol.pojo.Course;
import com.nichol.pojo.Student;
import com.nichol.pojo.Teacher;
import com.nichol.service.TeacherService;
import com.nichol.service.impl.TeacherServiceImpl;
import com.nichol.util.InputTrans;
import com.nichol.view.TeacherPage;

/*
 * @Author: 姜东浩
 * @Date: 2021-12-02 08:22:58
 * @Description: 教师控制器类
 */
public class TeacherController {

    Scanner sc = new Scanner(System.in);
    TeacherService teacherService = new TeacherServiceImpl();

    /**
     * @description: 教师登录
     * @param {String} id 用户名
     * @param {String} password 密码
     * @return {*}
     * @author: 姜东浩
     */
    public void login(String id, String password) {
        // 将传进来的账号密码封装为一个管理员对象
        Teacher teacher = new Teacher(id, password);

        // 登录成功显示首页视图，否则提示登陆失败
        if (teacherService.login(teacher) != null) {
            TeacherPage.showLoginSuccess();
        } else {
            System.out.println("ERROR : 登录失败!");
        }
    }

    /**
     * @description: 登分
     * @param {*}
     * @return {*} 操作提示信息
     * @author: 姜东浩
     */
    public String insertStudentGrade() {
        List<Student> studentList = teacherService.selectStudentList();
        for (int i = 0; i < studentList.size(); i++) {
            String sid = studentList.get(i).getStudentId();
            String sname = studentList.get(i).getSname();
            String ssex = studentList.get(i).getSex();
            double grade = studentList.get(i).getGrade();
            System.out.println("学生编号:" + sid + "  学生姓名:" + sname + "  学生性别:" + ssex + "  成绩:" + grade);
        }
        System.out.println("====输入exit退出登分页面");
        System.out.print("请输入学生ID：");
        String tsid = sc.next();
        if (tsid.equals("exit")) {
            return "";
        }

        boolean flag = false;
        for (int i = 0; i < studentList.size(); i++) {
            if(studentList.get(i).getStudentId().equals(tsid)){
                flag = true;
                break;
            }
        }

        if(flag ==false){
            return "ERROR: 此学生未选择您的课程!";
        }

        System.out.print("请输入学生成绩：");
        double sgrade = InputTrans.transitionToDouble(sc.next());
        if (teacherService.insertStudentGrade(tsid, sgrade) == 1) {
            return "SUCCESS: 录入成绩成功!";
        } else {
            return "ERROR: 录入成绩失败!";
        }
    }

    /**
     * @description: 改分
     * @param {*}
     * @return {*} 操作提示信息
     * @author: 姜东浩
     */
    public String updateStudentGrade() {
        List<Student> studentList = teacherService.selectStudentList();
        for (int i = 0; i < studentList.size(); i++) {
            String sid = studentList.get(i).getStudentId();
            String sname = studentList.get(i).getSname();
            String ssex = studentList.get(i).getSex();
            double grade = studentList.get(i).getGrade();
            System.out.println("学生编号:" + sid + "  学生姓名:" + sname + "  学生性别:" + ssex + "  成绩:" + grade);
        }
        System.out.println("====输入exit退出登分页面");
        System.out.print("请输入学生ID：");
        String tsid = sc.next();
        if (tsid.equals("exit")) {
            return "";
        }
        System.out.print("请输入学生成绩：");
        double sgrade = InputTrans.transitionToDouble(sc.next());
        if (teacherService.updateStudentGrade(tsid, sgrade) == 1) {
            return "SUCCESS: 修改成绩成功!";
        } else {
            return "ERROR: 修改成绩失败!";
        }
    }

    /**
     * @description: 查看学生集合
     * @param {*}
     * @return {*} 管理的学生集合
     * @author: 姜东浩
     */
    public void selectStudentList() {
        List<Student> studentList = teacherService.selectStudentList();
        for (int i = 0; i < studentList.size(); i++) {
            String sid = studentList.get(i).getStudentId();
            String sname = studentList.get(i).getSname();
            String ssex = studentList.get(i).getSex();
            double grade = studentList.get(i).getGrade();
            System.out.println("学生编号:" + sid + "  学生姓名:" + sname + "  学生性别:" + ssex + "  成绩:" + grade);
        }
    }

    /**
     * @description: 查看自己所授课程
     * @param {*}
     * @return {*}
     * @author: 姜东浩
     */    
    public void selectMyCourse() {
        Course course = teacherService.selectMyCourse();
        String cid = course.getId();
        String cname = course.getCname();
        int cnum = course.getNum();
        int csnum = course.getSnum();
        System.out.println("课程编号:" + cid + "  课程名:" + cname + "  已选人数:" + cnum + "  总容量:" + csnum);
    }

    /**
     * @description: 修改密码
     * @param {*}
     * @return {*} 操作提示系信息
     * @author: 姜东浩
     */    
    public String updatePassword(){
        System.out.print("请输入修改后的密码：");
        // 修改密码
        if (teacherService.updatePassword(sc.next()) == 1) {
            return "SUCCESS: 修改密码成功!";
        } else {
            return "ERROR: ID不存在,修改密码失败!";
        }
    }

}